library(tidyverse)
#Bars <- read.csv(file="flavors_of_cacao.csv", header=TRUE, sep=","); 

chocolateData <- read_csv("flavors_of_cacao.csv")

names(chocolateData) <- make.names(names(chocolateData), unique=TRUE)

topct <- function(x) { as.numeric( sub("\\D*([0-9.]+)\\D*","\\1",x) )/100 }

chocolateData$Cocoa.Percent <- topct(chocolateData$Cocoa.Percent)

colnames(chocolateData)[1] <- "Company"

chocolateData


#Przyk�adowe wykresy


plot(chocolateData$REF, chocolateData$Rating, type="p",col="red", pch=20, las=1)

boxplot(chocolateData$Cocoa.Percent)

plot(chocolateData$Rating, chocolateData$Cocoa.Percent, type="p",col="red", pch=20, las=1)

plot(chocolateData$REF, chocolateData$Cocoa.Percent, type="p",col="red", pch=20, las=1)

plot(chocolateData$REF, chocolateData$Rating, type="p",col="red", pch=20, las=1)

plot(chocolateData$Review.Date, chocolateData$Rating, type="p",col="red", pch=20, las=1)

plot(chocolateData$Review.Date, chocolateData$Cocoa.Percent, type="p",col="red", pch=20, las=1)



#filtr
library(sqldf)
dataframe <- subset(chocolateData, Review.Date == 2006 & Cocoa.Percent > 0.01)
dataframe

par(mar=c(16,6,4,1)+.1)

#Zale�no�� mediana ocen od nazwy firmy
library(dplyr)

x <- dataframe %>% 
  group_by(Company) %>% 
  summarise(Rating = median(Rating))

plot(x$Rating,type="h",xaxt="n", col="red", pch=20, las=1, xlab="", ylab="MedianofRatings")
axis(1, at=1:nrow(x), labels=x$Company, las=2)



#Zale�no�� median ocen od lokalizacji firmy
x <- dataframe %>% 
  group_by(Company.Location) %>% 
  summarise(Rating = median(Rating))


plot(x$Rating,type="h",xaxt="n", col="red", pch=20, las=1, xlab="", ylab="MedianOfRatings")
axis(1, at=1:nrow(x), labels=x$Company.Location, las=2)


#Zale�no�� mediany procent��w od lokalizacji firmy
x <- dataframe %>% 
  group_by(Company.Location) %>% 
  summarise(Cocoa.Percent = median(Cocoa.Percent))


plot(x$Cocoa.Percent,type="h",xaxt="n", col="red", pch=20, las=1, xlab="", ylab="MedianOfCocoaPercent")
axis(1, at=1:nrow(x), labels=x$Company.Location, las=2)


#Zale�no�� mediany ocen od lokalizacji pochodzenia (pa�stwa pochodzenia) ziaren
x2 <- dataframe %>% 
  group_by(Broad.Bean.Origin) %>% 
  summarise(Rating = median(Rating))


plot(x2$Rating,type="h",xaxt="n", col="red", pch=20, las=1, xlab="", ylab="MedianOfRating")
axis(1, at=1:nrow(x2), labels=x2$Broad.Bean.Origin, las=2)

#Zale�no�� mediany procent��w od lokalizacji pochodzenia (pa�stwa pochodzenia) ziaren
x2 <- chocolateData %>% 
  group_by(Broad.Bean.Origin) %>% 
  summarise(Cocoa.Percent = median(Cocoa.Percent))


plot(x2$Cocoa.Percent,type="h",xaxt="n", col="red", pch=20, las=1, xlab="", ylab="MedianOfCocoaPercent")
axis(1, at=1:nrow(x2), labels=x2$Broad.Bean.Origin, las=2)



#Zale�no�� liczby firm produkuj�cych czekolad� od kraju pochodzenia firmy
x3 <- chocolateData %>% 
  group_by(Company.Location) %>% 
  count("Company")

plot(x3$n,type="h",xaxt="n", col="red", pch=20, las=1, xlab="", ylab="NumberOfCompanies")
axis(1, at=1:nrow(x3), labels=x3$Company.Location, las=2)

#Zale�no�� liczby firm produkuj�cych czekolad� od kraju pochodzenia ziarna (bean)

x4 <- chocolateData %>% 
  group_by(Broad.Bean.Origin) %>% 
  count("Company")

plot(x4$n,type="h",xaxt="n", col="red", pch=20, las=1, xlab="", ylab="NumberOfCompanies")
axis(1, at=1:nrow(x4), labels=x4$Broad.Bean.Origin, las=2)


#Zale�no�� mediany ocen od nazwy firmy
x5 <- dataframe %>% 
  group_by(Company) %>% 
  summarise(Rating = median(Rating))


plot(x5$Rating,type="h",xaxt="n", col="red", pch=20, las=1, xlab="", ylab="MedianOfRating")
axis(1, at=1:nrow(x5), labels=x5$Company, las=2)

#Zale�no�� mediany procent��w od nazwy firmy
x6 <- dataframe %>% 
  group_by(Company) %>% 
  summarise(Cocoa.Percent = median(Cocoa.Percent))

plot(x6$Cocoa.Percent,type="h",xaxt="n", col="red", pch=20, las=1, xlab="", ylab="MedianOfCocoaPercent")
axis(1, at=1:nrow(x6), labels=x6$Company, las=2)


#Zale�no�� mediany REF od kraju siedziby firmy
x7 <- dataframe %>% 
  group_by(Company.Location) %>% 
  summarise(REF = median(REF))


plot(x7$REF,type="h",xaxt="n", col="red", pch=20, las=1, xlab="", ylab="MedianOfREF")
axis(1, at=1:nrow(x7), labels=x7$Company.Location, las=2)


#Zale�no�� mediany REF od kraju pochodzenie ziarna
x8 <- dataframe %>% 
  group_by(Broad.Bean.Origin) %>% 
  summarise(REF = median(REF))


plot(x8$REF,type="h",xaxt="n", col="red", pch=20, las=1, xlab="", ylab="MedianOfREF")
axis(1, at=1:nrow(x8), labels=x8$Broad.Bean.Origin, las=2)



#Zale�no�� mediany zawarto�ci procentowej oraz oceny od kraju siedziby firmy
x9 <- dataframe %>% 
  group_by(Company.Location) %>% 
  summarise(Rating = median(Rating))


x10 <- dataframe %>% 
  group_by(Company.Location) %>% 
  summarise(Cocoa.Percent = median(Cocoa.Percent))

barplot(rbind(x9$Rating, x10$Cocoa.Percent), ylim=c(0, 5), main="Warto�� Rating oraz Cocoa.Percent", col = c("darkblue","red"),  beside=TRUE)
axis(1, at=seq(1, 3*nrow(x9), by = 3), labels=x9$Company.Location, las=2)

legend("topright", 
       legend = c("Rating", "Cocoa.Percent"), 
       fill = c("darkblue", "red"))


#Zale�no�� mediany zawarto�ci procentowej oraz oceny od kraju pochodzenia ziarna
x11 <- dataframe %>% 
  group_by(Broad.Bean.Origin) %>% 
  summarise(Rating = median(Rating))


x12 <- dataframe %>% 
  group_by(Broad.Bean.Origin) %>% 
  summarise(Cocoa.Percent = median(Cocoa.Percent))

barplot(rbind(x11$Rating, x12$Cocoa.Percent), ylim=c(0, 5), main="Warto�� Rating oraz Cocoa.Percent", col = c("darkblue","red"),  beside=TRUE)
axis(1, at=seq(1, 3*nrow(x11), by = 3), labels=x11$Broad.Bean.Origin, las=2)

legend("topright", 
       legend = c("Rating", "Cocoa.Percent"), 
       fill = c("darkblue", "red"))


#Zale�no�� mediany oceny od typu ziarna
x13 <- dataframe %>% 
  group_by(Bean.Type) %>% 
  summarise(Rating = median(Rating))


plot(x13$Rating,type="h",xaxt="n", col="red", pch=20, las=1, xlab="", ylab="MedianOfRating")
axis(1, at=1:nrow(x13), labels=x13$Bean.Type, las=2)



#Zale�no�� mediany oceny od typu ziarna
x13 <- dataframe %>% 
  group_by(Bean.Type) %>% 
  summarise(Rating = median(Rating))


plot(x13$Rating,type="h",xaxt="n", col="red", pch=20, las=1, xlab="", ylab="MedianOfRating")
axis(1, at=1:nrow(x13), labels=x13$Bean.Type, las=2)


#Zale�no�� mediany zawarto�ci procentowej od typu ziarna
x14 <- dataframe %>% 
  group_by(Bean.Type) %>% 
  summarise(Cocoa.Percent = median(Cocoa.Percent))


plot(x14$Cocoa.Percent,type="h",xaxt="n", col="red", pch=20, las=1, xlab="", ylab="MedianOfCocoaPercent")
axis(1, at=1:nrow(x14), labels=x14$Bean.Type, las=2)


#Zale�no�� mediany ocen od daty
x15 <- chocolateData %>% 
  group_by(Review.Date) %>% 
  summarise(Rating = median(Rating))


plot(x15$Rating,type="h",xaxt="n", col="red", pch=20, las=1, xlab="", ylab="MedianOfRating")
axis(1, at=1:nrow(x15), labels=x15$Review.Date, las=2)


#Zale�no�� mediany zawarto��ci procentowej od daty
x16 <- chocolateData %>% 
  group_by(Review.Date) %>% 
  summarise(Cocoa.Percent = median(Cocoa.Percent))


plot(x16$Cocoa.Percent,type="h",xaxt="n", col="red", pch=20, las=1, xlab="", ylab="MedianOfCocoaPercent")
axis(1, at=1:nrow(x16), labels=x16$Review.Date, las=2)


#Zale�no�� liczby firm produkuj�cych czekolad� od rodzaju ziarna (bean)

x17 <- chocolateData %>% 
  group_by(Bean.Type) %>% 
  count("Company")

plot(x17$n,type="h",xaxt="n", col="red", pch=20, las=1, xlab="", ylab="NumberOfCompanies")
axis(1, at=1:nrow(x17), labels=x17$Bean.Type, las=2)

