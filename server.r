#server.r

library(lattice)
library(sqldf)

#preparing data and data cleansing

library(tidyverse)
#Bars <- read.csv(file="flavors_of_cacao.csv", header=TRUE, sep=","); 

chocolateData <- read_csv("flavors_of_cacao.csv")

names(chocolateData) <- make.names(names(chocolateData), unique=TRUE)

topct <- function(x) { as.numeric( sub("\\D*([0-9.]+)\\D*","\\1",x) )/100 }

chocolateData$Cocoa.Percent <- topct(chocolateData$Cocoa.Percent)

colnames(chocolateData)[1] <- "Company"




server <- function(input, output) {
  
  #Reactive valueII
  
  reactiveValueII <- reactive ({
    
    dataframePomII <- subset(chocolateData, Review.Date ==input$date  & Cocoa.Percent > input$percentageLimit)
    
    switch(input$xAxisII,
           
           "Kraj pochodzenia firmy" = switch(input$yAxisII,
                                                  
                                                  "Procentowa zawartosc kakao" = 
                                                  {
                                                    resultPomII <- dataframePomII %>% 
                                                      group_by(Company.Location) %>% 
                                                      summarise(Cocoa.Percent = median(Cocoa.Percent))
                                                    
                                                  },
                                                  
                                                  "Ocena tabliczki czekolady" = 
                                                  {
                                                    resultPomII <- dataframePomII %>% 
                                                      group_by(Company.Location) %>% 
                                                      summarise(Rating = median(Rating))
                                                    
                                                  }
                                                  
           ),
           
           "Kraj pochodzenia ziarna kakaowca" = switch(input$yAxisII,
                                                       "Procentowa zawartosc kakao" = 
                                                         
                                                       {
                                                         resultPomII <- dataframePomII %>% 
                                                           group_by(Broad.Bean.Origin) %>% 
                                                           summarise(Cocoa.Percent = median(Cocoa.Percent))
                                                         
                                                       },
                                                       
                                                       "Ocena tabliczki czekolady" = 
                                                       {
                                                         resultPomII <- dataframePomII %>% 
                                                           group_by(Broad.Bean.Origin) %>% 
                                                           summarise(Rating = median(Rating))
                                                         
                                                       }
           ), 
           
           
           "Typ ziarna kakaowca" = switch(input$yAxisII,
                                          
                                          "Procentowa zawartosc kakao" =
                                          {
                                            resultPomII <- dataframePomII %>% 
                                              group_by(Bean.Type) %>% 
                                              summarise(Cocoa.Percent = median(Cocoa.Percent))
                                            
                                          },
                                          
                                          
                                          "Ocena tabliczki czekolady" = 
                                          {
                                            resultPomII <- dataframePomII %>% 
                                              group_by(Bean.Type) %>% 
                                              summarise(Rating = median(Rating))
                                          }
           )
    )    
    
    resultPomII
  })
  
  
  #Reactive valueIII
  
  reactiveValueIII <- reactive ({
    
    switch(input$xAxisIII,
           
           "Data (Rok)" = switch(input$yAxisIII,
                                 
                                 "Procentowa zawartosc kakao" = 
                                 {
                                   resultPomIII <- chocolateData %>% 
                                     group_by(Review.Date) %>% 
                                     summarise(Cocoa.Percent = median(Cocoa.Percent))
                                   
                                 },
                                 
                                 "Ocena tabliczki czekolady" = 
                                 {
                                   resultPomIII <- chocolateData %>% 
                                     group_by(Review.Date) %>% 
                                     summarise(Rating = median(Rating))
                                   
                                 },
                                 
                                 "Liczba firm" =
                                 {
                                   resultPomIII <- chocolateData %>% 
                                     group_by(Review.Date) %>% 
                                     count("Company")
                                   
                                 }
                                 
           ),
           
           "Kraj pochodzenia firmy" = switch(input$yAxisIII,
                                                  
                                                  "Procentowa zawartosc kakao" = 
                                                  {
                                                    resultPomIII <- chocolateData %>% 
                                                      group_by(Company.Location) %>% 
                                                      summarise(Cocoa.Percent = median(Cocoa.Percent))
                                                    
                                                  },
                                                  
                                                  "Ocena tabliczki czekolady" = 
                                                  {
                                                    resultPomIII <- chocolateData %>% 
                                                      group_by(Company.Location) %>% 
                                                      summarise(Rating = median(Rating))
                                                    
                                                  },
                                                  
                                                  "Liczba firm" =
                                                  {
                                                    resultPomIII <- chocolateData %>% 
                                                      group_by(Company.Location) %>% 
                                                      count("Company")
                                                    
                                                  }
                                                  
           ),
           
           "Kraj pochodzenia ziarna kakaowca" = switch(input$yAxisIII,
                                                       
                                                       "Procentowa zawartosc kakao" = 
                                                       {
                                                         resultPomIII <- chocolateData %>% 
                                                           group_by(Broad.Bean.Origin) %>% 
                                                           summarise(Cocoa.Percent = median(Cocoa.Percent))
                                                         
                                                       },
                                                       
                                                       "Ocena tabliczki czekolady" = 
                                                       {
                                                         resultPomIII <- chocolateData %>% 
                                                           group_by(Broad.Bean.Origin) %>% 
                                                           summarise(Rating = median(Rating))
                                                         
                                                         
                                                       },
                                                       
                                                       "Liczba firm" =
                                                       {
                                                         resultPomIII <- chocolateData %>% 
                                                           group_by(Broad.Bean.Origin) %>% 
                                                           count("Company")
                                                         
                                                       }
                                                       
           ),
           
           
           "Typ ziarna kakaowca" =  switch(input$yAxisIII,
                                           
                                           "Procentowa zawartosc kakao" = 
                                           {
                                             resultPomIII <- chocolateData %>% 
                                               group_by(Bean.Type) %>% 
                                               summarise(Cocoa.Percent = median(Cocoa.Percent))
                                             
                                             
                                           },
                                           
                                           "Ocena tabliczki czekolady" = 
                                           {
                                             resultPomIII <- chocolateData %>% 
                                               group_by(Bean.Type) %>% 
                                               summarise(Rating = median(Rating))
                                             
                                             
                                             
                                           },
                                           
                                           "Liczba firm" =
                                           {
                                             resultPomIII <- chocolateData %>% 
                                               group_by(Bean.Type) %>% 
                                               count("Company")
                                             
                                           }
                                           
           )
    )
    
    
    resultPomIII
  })
  
  
  #statystyki dla 1 karty
  output$stats1 <- renderPrint({
    
    switch(input$xAxisI,
           "Procentowa zawartosc kakao" = summary(chocolateData$Cocoa.Percent),
           "Ocena tabliczki czekolady"= summary(chocolateData$Rating)
    )
  })
  
  #statystyki dla 2 karty
  output$stats2 <- renderPrint({
    
    switch(input$yAxisII,
           "Procentowa zawartosc kakao" = summary(reactiveValueII()$Cocoa.Percent),
           "Ocena tabliczki czekolady"= summary(reactiveValueII()$Rating)
    )
    
  })
  
  #statystyki dla 3 karty
  output$stats3 <- renderPrint({
    
    switch(input$yAxisIII,
           "Procentowa zawartosc kakao" = summary(reactiveValueIII()$Cocoa.Percent),
           "Ocena tabliczki czekolady"= summary(reactiveValueIII()$Rating),
           "Liczba firm" = summary(reactiveValueIII()$n)
    )
    
  })
  
  #wykres 1 zmiennej
  output$bwplot <- renderPlot({
    
    switch(input$xAxisI,
           "Procentowa zawartosc kakao" = bwplot(chocolateData$Cocoa.Percent, xlab = "Procent" , main = "Procentowa zawartosc kakao w tabliczce czekolady dla lat 2006 - 2017"), 
           "Ocena tabliczki czekolady" = bwplot(chocolateData$Rating, xlab = "Ocena", main = "Ocena tabliczki czekolady w latach 2006 - 2017") 
    )
    
  })
  
  
  #wykres 2 zmiennych dla danego roku
  output$plotIIAxis<- renderPlot({
    
    #utworzenie odpowiedniej ramki danych na podstawie wartosci z suwakow
    dataframe <- subset(chocolateData, Review.Date ==input$date  & Cocoa.Percent > input$percentageLimit)
    
    par(mar=c(12,6,4,1)+.1)
    
    
    switch(input$xAxisII,
           
           "Kraj pochodzenia firmy" = switch(input$yAxisII,
                                                  
                                                  "Procentowa zawartosc kakao" = 
                                                  {
                                                    result <- dataframe %>% 
                                                      group_by(Company.Location) %>% 
                                                      summarise(Cocoa.Percent = median(Cocoa.Percent))
                                                    
                                                    plot(result$Cocoa.Percent,type="h",xaxt="n", col="red", pch=20, xlab = "", ylab="", main = "Zależność mediany zawartości procentowych kakao od kraju pochodzenia firmy")
                                                    axis(1, at=1:nrow(result), labels=result$Company.Location, las=2)
                                                  },
                                                  
                                                  "Ocena tabliczki czekolady" = 
                                                  {
                                                    result <- dataframe %>% 
                                                      group_by(Company.Location) %>% 
                                                      summarise(Rating = median(Rating))
                                                    
                                                    
                                                    plot(result$Rating,type="h",xaxt="n", col="red", pch=20, xlab = "", ylab="", main = "Zależność mediany ocen od kraju pochodzenia firmy")
                                                    axis(1, at=1:nrow(result), labels=result$Company.Location, las=2)
                                                    
                                                  }
                                                  
           ),
           
           "Kraj pochodzenia ziarna kakaowca" = switch(input$yAxisII,
                                                       "Procentowa zawartosc kakao" = 
                                                         
                                                       {
                                                         result <- dataframe %>% 
                                                           group_by(Broad.Bean.Origin) %>% 
                                                           summarise(Cocoa.Percent = median(Cocoa.Percent))
                                                         
                                                         
                                                         plot(result$Cocoa.Percent,type="h",xaxt="n", col="red", pch=20, xlab = "", ylab="", main = "Zależność mediany zawartości procentowych kakao od kraju pochodzenia ziarna kakaowca")
                                                         axis(1, at=1:nrow(result), labels=result$Broad.Bean.Origin, las=2)
                                                         
                                                       },
                                                       
                                                       "Ocena tabliczki czekolady" = 
                                                       {
                                                         result <- dataframe %>% 
                                                           group_by(Broad.Bean.Origin) %>% 
                                                           summarise(Rating = median(Rating))
                                                         
                                                         
                                                         plot(result$Rating,type="h",xaxt="n", col="red", pch=20, xlab = "", ylab="", main = "Zależność mediany ocen od kraju pochodzenia ziarna kakaowca")
                                                         axis(1, at=1:nrow(result), labels=result$Broad.Bean.Origin, las=2)
                                                       }
           ), 
           
           
           "Typ ziarna kakaowca" = switch(input$yAxisII,
                                          
                                          "Procentowa zawartosc kakao" =
                                          {
                                            result <- dataframe %>% 
                                              group_by(Bean.Type) %>% 
                                              summarise(Cocoa.Percent = median(Cocoa.Percent))
                                            
                                            plot(result$Cocoa.Percent,type="h",xaxt="n", col="red", pch=20, xlab = "", ylab="", main = "Zależność mediany zawartości procentowych kakao od typu ziarna kakaowca")
                                            axis(1, at=1:nrow(result), labels=result$Bean.Type, las=2)
                                          },
                                          
                                          
                                          "Ocena tabliczki czekolady" = 
                                          {
                                            result <- dataframe %>% 
                                              group_by(Bean.Type) %>% 
                                              summarise(Rating = median(Rating))
                                            
                                            plot(result$Rating,type="h",xaxt="n", col="red", pch=20, xlab = "", ylab="", main = "Zależność mediany ocen od typu ziarna kakaowca")
                                            axis(1, at=1:nrow(result), labels=result$Bean.Type, las=2)
                                          }
           )
    )
    
    
  })
  
  
  #wykres 2 zmiennych dla lat 2006-2017
  output$plotIIIAxis<- renderPlot({
    
    par(mar=c(13,6,4,1)+.1)
    
    switch(input$xAxisIII,
           
           "Data (Rok)" = switch(input$yAxisIII,
                                 
                                 "Procentowa zawartosc kakao" = 
                                 {
                                   result <- chocolateData %>% 
                                     group_by(Review.Date) %>% 
                                     summarise(Cocoa.Percent = median(Cocoa.Percent))
                                   
                                   plot(result$Cocoa.Percent,type="h",xaxt="n", col="red", pch=20, xlab = "", ylab="", main = "Zależność mediany zawartości procentowych kakao od daty(roku)")
                                   axis(1, at=1:nrow(result), labels=result$Review.Date, las=2)
                                 },
                                 
                                 "Ocena tabliczki czekolady" = 
                                 {
                                   result <- chocolateData %>% 
                                     group_by(Review.Date) %>% 
                                     summarise(Rating = median(Rating))
                                   
                                   
                                   plot(result$Rating,type="h",xaxt="n", col="red", pch=20, xlab = "", ylab="", main = "Zależność mediany ocen od daty(roku)")
                                   axis(1, at=1:nrow(result), labels=result$Review.Date, las=2)
                                   
                                 },
                                 
                                 "Liczba firm" =
                                 {
                                   result <- chocolateData %>% 
                                     group_by(Review.Date) %>% 
                                     count("Company")
                                   
                                   
                                   plot(result$n,type="h",xaxt="n", col="red", pch=20, xlab = "", ylab="", main = "Zależność liczby firm produkujących czekoladę od daty(roku)")
                                   axis(1, at=1:nrow(result), labels=result$Review.Date, las=2)
                                 }
                                 
           ),
           
           "Kraj pochodzenia firmy" = switch(input$yAxisIII,
                                                  
                                                  "Procentowa zawartosc kakao" = 
                                                  {
                                                    result <- chocolateData %>% 
                                                      group_by(Company.Location) %>% 
                                                      summarise(Cocoa.Percent = median(Cocoa.Percent))
                                                    
                                                    plot(result$Cocoa.Percent,type="h",xaxt="n", col="red", pch=20, xlab = "", ylab="", main = "Zależność mediany zawartości procentowych kakao od kraju pochodzenia firmy")
                                                    axis(1, at=1:nrow(result), labels=result$Company.Location, las=2)
                                                  },
                                                  
                                                  "Ocena tabliczki czekolady" = 
                                                  {
                                                    result <- chocolateData %>% 
                                                      group_by(Company.Location) %>% 
                                                      summarise(Rating = median(Rating))
                                                    
                                                    
                                                    plot(result$Rating,type="h",xaxt="n", col="red", pch=20, xlab = "", ylab="", main = "Zależność mediany ocen od kraju pochodzenia firmy")
                                                    axis(1, at=1:nrow(result), labels=result$Company.Location, las=2)
                                                    
                                                  },
                                                  
                                                  "Liczba firm" =
                                                  {
                                                    result <- chocolateData %>% 
                                                      group_by(Company.Location) %>% 
                                                      count("Company")
                                                    
                                                    
                                                    plot(result$n,type="h",xaxt="n", col="red", pch=20, xlab = "", ylab="", main = "Liczba firm produkujących czekoladę w danym kraju")
                                                    axis(1, at=1:nrow(result), labels=result$Company.Location, las=2)
                                                  }
                                                  
           ),
           
           "Kraj pochodzenia ziarna kakaowca" = switch(input$yAxisIII,
                                                       
                                                       "Procentowa zawartosc kakao" = 
                                                       {
                                                         result <- chocolateData %>% 
                                                           group_by(Broad.Bean.Origin) %>% 
                                                           summarise(Cocoa.Percent = median(Cocoa.Percent))
                                                         
                                                         plot(result$Cocoa.Percent,type="h",xaxt="n", col="red", pch=20, xlab = "", ylab="", main = "Zależność mediany zawartości procentowych kakao od kraju pochodzenia ziarna")
                                                         axis(1, at=1:nrow(result), labels=result$Broad.Bean.Origin, las=2)
                                                       },
                                                       
                                                       "Ocena tabliczki czekolady" = 
                                                       {
                                                         result <- chocolateData %>% 
                                                           group_by(Broad.Bean.Origin) %>% 
                                                           summarise(Rating = median(Rating))
                                                         
                                                         
                                                         plot(result$Rating,type="h",xaxt="n", col="red", pch=20, xlab = "", ylab="", main = "Zależność mediany ocen od kraju pochodzenia ziarna")
                                                         axis(1, at=1:nrow(result), labels=result$Broad.Bean.Origin, las=2)
                                                         
                                                       },
                                                       
                                                       "Liczba firm" =
                                                       {
                                                         result <- chocolateData %>% 
                                                           group_by(Broad.Bean.Origin) %>% 
                                                           count("Company")
                                                         
                                                         
                                                         plot(result$n,type="h",xaxt="n", col="red", pch=20, xlab = "", ylab="", main = "Liczba firm, ktore korzystają z ziaren pochodzących z danego kraju")
                                                         axis(1, at=1:nrow(result), labels=result$Broad.Bean.Origin, las=2)
                                                       }
                                                       
           ),
           
           
           "Typ ziarna kakaowca" =  switch(input$yAxisIII,
                                           
                                           "Procentowa zawartosc kakao" = 
                                           {
                                             result <- chocolateData %>% 
                                               group_by(Bean.Type) %>% 
                                               summarise(Cocoa.Percent = median(Cocoa.Percent))
                                             
                                             plot(result$Cocoa.Percent,type="h",xaxt="n", col="red", pch=20, xlab = "", ylab="", main = "Zależność mediany zawartości procentowych kakao od typu ziarna kakaowca")
                                             axis(1, at=1:nrow(result), labels=result$Bean.Type, las=2)
                                           },
                                           
                                           "Ocena tabliczki czekolady" = 
                                           {
                                             result <- chocolateData %>% 
                                               group_by(Bean.Type) %>% 
                                               summarise(Rating = median(Rating))
                                             
                                             
                                             plot(result$Rating,type="h",xaxt="n", col="red", pch=20, xlab = "", ylab="", main = "Zależność mediany ocen od typu ziarna kakaowca")
                                             axis(1, at=1:nrow(result), labels=result$Bean.Type, las=2)
                                             
                                           },
                                           
                                           "Liczba firm" =
                                           {
                                             result <- chocolateData %>% 
                                               group_by(Bean.Type) %>% 
                                               count("Company")
                                             
                                             
                                             plot(result$n,type="h",xaxt="n", col="red", pch=20, xlab = "", ylab="", main = "Liczba firm korzystająca z ziaren danego typu")
                                             axis(1, at=1:nrow(result), labels=result$Bean.Type, las=2)
                                           }
                                           
           )
    )
    
    
  })
  
  
  
  
}