#ui.r
Sys.setlocale(category = "LC_ALL", locale = "Polish")

library(shiny)

ui <- fluidPage(
  
  includeCSS("file.css"),
  
  tabsetPanel(
    
    tabPanel("Wykres jednej zmiennej dla lat 2006 - 2017",
             
             wellPanel(
               tags$h4("Projekt z przedmiotu",  tags$em("Analiza i wizualizacja danych")),
               tags$h5(tags$strong("Temat:"), tags$em("Statystyki jakości oraz produkcji czekolady")),
               tags$hr(),
               tags$h5(tags$strong("Autor:"), tags$em("Jakub Kapusta I6B2S1")),
               tags$hr(),
               tags$a(href = "https://www.kaggle.com/rtatman/chocolate-bar-ratings#flavors_of_cacao.csv", "Źródło danych")
             ),
             
             wellPanel(
               #ustawienia
               fluidRow(
                 column(4,offset=4, selectInput(inputId = "xAxisI", label = "Wybierz pierwszą zmienną: ", choices = c("Procentowa zawartosc kakao", "Ocena tabliczki czekolady")))
               )
             ),
             
             wellPanel(
               fluidRow(
                 column(10,offset=1, plotOutput("bwplot"))
               )
             ),
             
             fluidRow(
               verbatimTextOutput("stats1")
             )
             
    ),
    
    tabPanel("Wykres dwóch zmiennych dla wybranego roku",
             
             wellPanel(
               tags$h4("Projekt z przedmiotu",  tags$em("Analiza i wizualizacja danych")),
               tags$h5(tags$strong("Temat:"), tags$em("Statystyki jakości oraz produkcji czekolady")),
               tags$hr(),
               tags$h5(tags$strong("Autor:"), tags$em("Jakub Kapusta I6B2S1")),
               tags$hr(),
               tags$a(href = "https://www.kaggle.com/rtatman/chocolate-bar-ratings#flavors_of_cacao.csv", "Źródło danych")
             ),
             
             wellPanel(
               
               #ustawienia
               fluidRow(
                 column(4, offset=2, selectInput(inputId = "xAxisII", label = "Wybierz pierwszą zmienną: ", choices = c("Kraj pochodzenia firmy", "Kraj pochodzenia ziarna kakaowca", "Typ ziarna kakaowca"))),
                 column(4, selectInput(inputId = "yAxisII", label = "Wybierz drugą zmienną: ", choices = c("Procentowa zawartosc kakao", "Ocena tabliczki czekolady")))
               )
             ),
             
             wellPanel(
               #suwaki
               fluidRow(
                 column(4,offset=2, sliderInput(inputId = "date", label="Wybierz rok dla statystyki:", value=1, min=2006, max=2017)),
                 column(4, sliderInput(inputId = "percentageLimit", label="Wybierz minimalną zawartość kakao w czekoladzie:", value = 0.01, min=0, max=1))
               )
             ),
             
             wellPanel(
               fluidRow(
                 column(10, offset=1, plotOutput("plotIIAxis"))
               )
             ),   
             fluidRow(
               verbatimTextOutput("stats2")
             )
             
             
             
    ),
    
    tabPanel("Wykresy dwóch zmiennych dla lat 2006-2017",
             
             wellPanel(
               tags$h4("Projekt z przedmiotu",  tags$em("Analiza i wizualizacja danych")),
               tags$h5(tags$strong("Temat:"), tags$em("Statystyki jakości oraz produkcji czekolady")),
               tags$hr(),
               tags$h5(tags$strong("Autor:"), tags$em("Jakub Kapusta I6B2S1")),
               tags$hr(),
               tags$a(href = "https://www.kaggle.com/rtatman/chocolate-bar-ratings#flavors_of_cacao.csv", "Źródło danych")
             ),
             
             wellPanel(
               fluidRow(
                 column(4, offset=2, selectInput(inputId = "xAxisIII", label = "Wybierz pierwszą zmienną:  ", choices = c("Data (Rok)", "Kraj pochodzenia firmy", "Kraj pochodzenia ziarna kakaowca", "Typ ziarna kakaowca"))),
                 column(4,  selectInput(inputId = "yAxisIII", label = "Wybierz drugą zmienną: ", choices = c("Procentowa zawartosc kakao", "Ocena tabliczki czekolady", "Liczba firm")))
               )
             ),
             wellPanel(
               fluidRow(
                 column(12, plotOutput("plotIIIAxis"))
               )
             ),
             
             fluidRow(
               verbatimTextOutput("stats3")
             )
    )
    
    
    
  )
  
)
